use cant_stop::{Gain, SingleSolver};

fn main() {
    let mut th = 1.0;
    loop {
        let solver = SingleSolver::new(th);
        println!("{} {}", th, solver.solve(&Gain::new()));
        th /= 2.0;
    }
}
