use crate::{expr::Expr, DicingState, Gain, IntProb, DENOMINATOR};

fn as_p(p: IntProb) -> f64 {
    p as f64 / DENOMINATOR as f64
}

pub struct SingleSolver {
    th: f64,
}

impl SingleSolver {
    pub fn new(th: f64) -> SingleSolver {
        SingleSolver { th }
    }

    /// `gain` から開始した場合の終了ターン数期待値を求める
    pub fn solve(&self, gain: &Gain) -> f64 {
        self.solve_start(gain, 1.0)
    }

    fn solve_start(&self, gain: &Gain, factor: f64) -> f64 {
        if gain.is_finished() {
            return 0.0;
        }
        if factor < self.th {
            return self.simple_finish_turn(gain) * factor;
        }
        let state = DicingState::new(gain.clone());
        let e = self.calc_go(&state, factor);
        e.bisect()
    }

    pub fn simple_finish_turn(&self, gain: &Gain) -> f64 {
        const P: [IntProb; 11] = [171, 302, 461, 580, 727, 834, 727, 580, 461, 302, 171];
        let mut e = P
            .iter()
            .zip(gain.inner())
            .map(|(&p, &g)| g as f64 * 1296.0 / p as f64)
            .collect::<Vec<_>>();
        e.sort_by(|a, b| a.partial_cmp(b).unwrap());
        (e[0] + e[1] + e[2]) / 10.0
    }

    fn solve_on_the_way(&self, state: &DicingState, factor: f64) -> Expr {
        if state.gain.is_finished() {
            return Expr::zero();
        }
        // 停止して次のターンを迎える必要があるので +1ターンが加算される(factorがかかるけど)
        let stop = Expr::linear(0.0, factor + self.solve_start(&state.gain, factor));
        if factor < self.th {
            stop
        } else {
            self.calc_go(state, factor).min(stop)
        }
    }

    fn calc_go(&self, state: &DicingState, factor: f64) -> Expr {
        let mut go = Expr::zero();
        for (next, p) in state.list_all_possibilities() {
            let factor = factor * as_p(p);
            if next.is_empty() {
                // 失敗なので、現在地点そのものの期待値+1ターンが必要
                go = go + Expr::linear(factor, factor);
            } else {
                // 最良のものを1つ選ぶ
                let mut min = self.solve_on_the_way(&next[0], factor);
                for n in next.iter().skip(1) {
                    min = min.min(self.solve_on_the_way(n, factor));
                }
                go = go + min;
            }
        }
        go
    }
}
