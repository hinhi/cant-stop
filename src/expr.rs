//! self-consistent な期待値を求めるための計算機
//!
//! 「Can't stop」は Go を選んで失敗した場合はスタート地点に戻るので、
//! スタート地点からの期待値計算にスタート地点からの期待値そのものを含む。
//! （これを差して self-consistent と呼んでいる）
//!
//! ## Example
//! ```text
//! f(x) := min(x/4 + 3/2, 2)
//! solve: x = f(x)
//! ```
//! ↓
//! ```rust
//! # use cant_stop::expr::Expr;
//! let e = Expr::linear(1.0 / 4.0, 3.0 / 2.0).min(Expr::linear(0.0, 3.0));
//! assert_eq!(e.bisect(), 2.0);
//! ```
use std::{
    fmt::{Display, Formatter},
    ops::{Add, Div, Mul},
};

#[derive(Debug, Clone)]
pub struct Sum {
    one: f64,
    zero: f64,
    nonlinear: Vec<Min>,
}

#[derive(Debug, Clone)]
pub struct Min {
    sum: Vec<Sum>,
}

#[derive(Debug, Clone)]
pub enum Expr {
    Sum(Sum),
    Min(Min),
}

impl Add<Sum> for Sum {
    type Output = Sum;
    fn add(self, mut rhs: Sum) -> Sum {
        let one = self.one + rhs.one;
        let zero = self.zero + rhs.zero;
        let mut nonlinear = self.nonlinear;
        nonlinear.append(&mut rhs.nonlinear);
        Sum {
            one,
            zero,
            nonlinear,
        }
    }
}

impl Add<Min> for Min {
    type Output = Sum;
    fn add(self, rhs: Min) -> Sum {
        Sum {
            one: 0.0,
            zero: 0.0,
            nonlinear: vec![self, rhs],
        }
    }
}

impl Mul<f64> for Sum {
    type Output = Sum;
    fn mul(self, rhs: f64) -> Sum {
        Sum {
            one: self.one * rhs,
            zero: self.zero * rhs,
            nonlinear: self.nonlinear.into_iter().map(|s| s * rhs).collect(),
        }
    }
}

impl Mul<f64> for Min {
    type Output = Min;
    fn mul(self, rhs: f64) -> Min {
        Min {
            sum: self.sum.into_iter().map(|s| s * rhs).collect(),
        }
    }
}

impl Div<f64> for Sum {
    type Output = Sum;
    fn div(self, rhs: f64) -> Sum {
        Sum {
            one: self.one / rhs,
            zero: self.zero / rhs,
            nonlinear: self.nonlinear.into_iter().map(|s| s / rhs).collect(),
        }
    }
}

impl Div<f64> for Min {
    type Output = Min;
    fn div(self, rhs: f64) -> Min {
        Min {
            sum: self.sum.into_iter().map(|s| s / rhs).collect(),
        }
    }
}

impl Add<Expr> for Expr {
    type Output = Expr;
    fn add(self, rhs: Expr) -> Expr {
        use Expr::*;
        match (self, rhs) {
            (Sum(a), Sum(b)) => Sum(a + b),
            (Min(a), Min(b)) => Sum(a + b),
            (Sum(mut s), Min(m)) | (Min(m), Sum(mut s)) => {
                s.nonlinear.push(m);
                Sum(s)
            }
        }
    }
}

impl Mul<f64> for Expr {
    type Output = Expr;
    fn mul(self, rhs: f64) -> Expr {
        match self {
            Expr::Sum(s) => Expr::Sum(s * rhs),
            Expr::Min(m) => Expr::Min(m * rhs),
        }
    }
}

impl Div<f64> for Expr {
    type Output = Expr;
    fn div(self, rhs: f64) -> Expr {
        match self {
            Expr::Sum(s) => Expr::Sum(s / rhs),
            Expr::Min(m) => Expr::Min(m / rhs),
        }
    }
}

impl Display for Sum {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let mut head = true;
        if self.one != 0.0 {
            f.write_fmt(format_args!("{}x", self.one))?;
            head = false;
        }
        if self.zero != 0.0 {
            if !head {
                f.write_str(" + ")?;
            }
            f.write_fmt(format_args!("{}", self.zero))?;
            head = false;
        }
        if !self.nonlinear.is_empty() {
            for s in self.nonlinear.iter() {
                if !head {
                    f.write_str(" + ")?;
                }
                Display::fmt(s, f)?;
                head = false;
            }
        }
        Ok(())
    }
}

impl Display for Min {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str("min(")?;
        self.sum[0].fmt(f)?;
        for s in self.sum.iter().skip(1) {
            f.write_str(", ")?;
            Display::fmt(s, f)?;
        }
        f.write_str(")")
    }
}

impl Display for Expr {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Expr::Sum(s) => Display::fmt(s, f),
            Expr::Min(m) => Display::fmt(m, f),
        }
    }
}

impl Sum {
    pub fn eval(&self, x: f64) -> f64 {
        self.one * x + self.zero + self.nonlinear.iter().map(|m| m.eval(x)).sum::<f64>()
    }

    pub fn is_constant(&self) -> bool {
        self.one == 0.0 && self.nonlinear.is_empty()
    }
}

impl Min {
    pub fn eval(&self, x: f64) -> f64 {
        let mut m = f64::MAX;
        for s in self.sum.iter() {
            let y = s.eval(x);
            if y < m {
                m = y;
            }
        }
        m
    }
}

impl Expr {
    pub const fn zero() -> Expr {
        Expr::linear(0.0, 0.0)
    }

    pub const fn linear(x: f64, a: f64) -> Expr {
        Expr::Sum(Sum {
            one: x,
            zero: a,
            nonlinear: Vec::new(),
        })
    }

    pub fn min(self, other: Expr) -> Expr {
        match (self, other) {
            (Expr::Sum(a), Expr::Sum(b)) => {
                if a.is_constant() && b.is_constant() {
                    Expr::linear(0.0, a.zero.min(b.zero))
                } else {
                    Expr::Min(Min { sum: vec![a, b] })
                }
            }
            (Expr::Min(mut a), Expr::Min(mut b)) => {
                a.sum.append(&mut b.sum);
                Expr::Min(a)
            }
            (Expr::Sum(s), Expr::Min(mut m)) | (Expr::Min(mut m), Expr::Sum(s)) => {
                m.sum.push(s);
                Expr::Min(m)
            }
        }
    }

    pub fn eval(&self, x: f64) -> f64 {
        match self {
            Expr::Sum(s) => s.eval(x),
            Expr::Min(m) => m.eval(x),
        }
    }

    pub fn bisect(&self) -> f64 {
        let mut lo = 1.0;
        while self.eval(lo) < lo {
            lo /= 2.0
        }
        let mut hi = lo * 2.0;
        while hi < self.eval(hi) {
            hi *= 2.0;
        }
        while hi - lo > 1e-12 {
            let mid = (hi + lo) / 2.0;
            let v = self.eval(mid);
            if v < mid {
                hi = mid;
            } else {
                lo = mid;
            }
        }
        hi
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_eval() {
        let e = Expr::linear(2.0, 3.0);
        assert_eq!(e.eval(1.0), 5.0);
        assert_eq!(e.eval(3.0), 9.0);
        let e = Expr::linear(1.0, 2.0).min(Expr::linear(2.0, 0.5));
        assert_eq!(e.eval(0.0), 0.5);
        assert_eq!(e.eval(1.0), 2.5);
        assert_eq!(e.eval(2.0), 4.0);
    }
}
