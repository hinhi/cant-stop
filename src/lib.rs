mod board;
pub mod expr;
mod game;
mod single_solver;

use std::collections::HashMap;

use lazy_static::lazy_static;
use rand::{seq::SliceRandom, Rng};

pub use board::*;
pub use game::*;
pub use single_solver::SingleSolver;

pub type IntProb = u64;
pub const DENOMINATOR: IntProb = 6 * 6 * 6 * 6;

lazy_static! {
    pub static ref DICE_INFO: Vec<DiceInfo> = list_dice();
    pub static ref PROB_TABLE: Vec<usize> = make_prob_table(&DICE_INFO);
}

/// 4つのダイスを2つずつペアにした状態を管理する。
/// 内部的にペアはソートされた状態になる。
///
/// ## Example
///
/// ```rust
/// # use cant_stop::DicePair;
/// assert_eq!(DicePair::new(3, 5), DicePair::new(5, 3));
/// assert_eq!(DicePair::new(10, 2).first(), 2);
/// assert_eq!(DicePair::new(3, 9).second(), 9);
/// ```
#[derive(Debug, Copy, Clone, PartialOrd, PartialEq, Ord, Eq, Hash)]
pub struct DicePair(u8, u8);

impl DicePair {
    pub const fn new(d0: u8, d1: u8) -> DicePair {
        if d0 < d1 {
            DicePair(d0, d1)
        } else {
            DicePair(d1, d0)
        }
    }

    pub const fn first(self) -> u8 {
        self.0
    }

    pub const fn second(self) -> u8 {
        self.1
    }
}

#[derive(Debug, Clone, PartialOrd, PartialEq, Ord, Eq)]
pub struct DiceInfo {
    pub dice_pairs: Vec<DicePair>,
    pub probability: IntProb,
}

fn list_dice() -> Vec<DiceInfo> {
    let mut m = HashMap::new();
    for d1 in 1..=6 {
        for d2 in 1..=6 {
            for d3 in 1..=6 {
                for d4 in 1..=6 {
                    let pair1 = DicePair::new(d1 + d2, d3 + d4);
                    let pair2 = DicePair::new(d1 + d3, d2 + d4);
                    let pair3 = DicePair::new(d1 + d4, d2 + d3);
                    let mut pairs = if pair1 != pair2 {
                        if pair1 != pair3 && pair2 != pair3 {
                            vec![pair1, pair2, pair3]
                        } else {
                            vec![pair1, pair2]
                        }
                    } else if pair1 != pair3 {
                        vec![pair1, pair3]
                    } else {
                        vec![pair1]
                    };
                    pairs.sort();
                    *m.entry(pairs).or_insert(0) += 1;
                }
            }
        }
    }
    m.into_iter()
        .map(|(v, p)| DiceInfo {
            dice_pairs: v,
            probability: p,
        })
        .collect()
}

fn make_prob_table(dice_info: &[DiceInfo]) -> Vec<usize> {
    let mut table = Vec::with_capacity(6 * 6 * 6 * 6);
    for (i, info) in dice_info.iter().enumerate() {
        for _ in 0..info.probability {
            table.push(i);
        }
    }
    table
}

pub fn roll_dice<R: Rng>(rng: &mut R) -> &[DicePair] {
    &DICE_INFO[*PROB_TABLE.choose(rng).unwrap()].dice_pairs
}
