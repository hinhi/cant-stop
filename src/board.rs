use std::ops::{Index, IndexMut};

use fnv::FnvHashMap;

use crate::{DicePair, IntProb, DICE_INFO};

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct Gain([u8; 11]);

impl Index<u8> for Gain {
    type Output = u8;
    fn index(&self, index: u8) -> &Self::Output {
        &self.0[(index - 2) as usize]
    }
}

impl IndexMut<u8> for Gain {
    fn index_mut(&mut self, index: u8) -> &mut Self::Output {
        &mut self.0[(index - 2) as usize]
    }
}

impl Default for Gain {
    fn default() -> Self {
        Gain::new()
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub enum Climbing {
    Zero,
    One(u8),
    Two(u8, u8),
    Three(u8, u8, u8),
}

impl Default for Climbing {
    fn default() -> Climbing {
        Climbing::Zero
    }
}

impl Climbing {
    /// ダイスを振る
    ///
    /// ## Example
    ///
    /// ```rust
    /// # use cant_stop::Climbing;
    /// let a = Climbing::default();
    /// let b = a.apply(7);
    /// assert_eq!(b, Some(Climbing::One(7)));
    /// ```
    pub fn apply(self, dice: u8) -> Option<Climbing> {
        use std::cmp::Ordering::*;
        match self {
            Climbing::Zero => Some(Climbing::One(dice)),
            Climbing::One(a) => match a.cmp(&dice) {
                Equal => Some(self),
                Less => Some(Climbing::Two(a, dice)),
                Greater => Some(Climbing::Two(dice, a)),
            },
            Climbing::Two(a, b) => {
                if a == dice || b == dice {
                    Some(self)
                } else if b < dice {
                    Some(Climbing::Three(a, b, dice))
                } else if dice < a {
                    Some(Climbing::Three(dice, a, b))
                } else {
                    Some(Climbing::Three(a, dice, b))
                }
            }
            Climbing::Three(a, b, c) => {
                if a == dice || b == dice || c == dice {
                    Some(self)
                } else {
                    None
                }
            }
        }
    }

    /// Climbing::Zero なら true
    ///
    /// ```rust
    /// # use cant_stop::Climbing;
    /// assert!(Climbing::default().is_zero());
    /// ```
    pub const fn is_zero(self) -> bool {
        matches!(self, Climbing::Zero)
    }

    /// Climbing::One なら true
    ///
    /// ```rust
    /// # use cant_stop::Climbing;
    /// let c = Climbing::default().apply(7).unwrap();
    /// assert!(c.is_one());
    /// ```
    pub const fn is_one(self) -> bool {
        matches!(self, Climbing::One(_))
    }

    /// Climbing::Two なら true
    ///
    /// ```rust
    /// # use cant_stop::Climbing;
    /// let c = Climbing::default().apply(7).unwrap();
    /// let c = c.apply(8).unwrap();
    /// assert!(c.is_two());
    /// ```
    pub const fn is_two(self) -> bool {
        matches!(self, Climbing::Two(_, _))
    }

    /// Climbing::Three なら true
    ///
    /// ```rust
    /// # use cant_stop::Climbing;
    /// let c = Climbing::default().apply(7).unwrap();
    /// let c = c.apply(8).unwrap();
    /// let c = c.apply(9).unwrap();
    /// assert!(c.is_three());
    /// ```
    pub const fn is_three(self) -> bool {
        matches!(self, Climbing::Three(_, _, _))
    }
}

impl Gain {
    #[inline]
    pub const fn new() -> Gain {
        Gain([3, 5, 7, 9, 11, 13, 11, 9, 7, 5, 3])
    }

    #[inline]
    pub const fn is_climbed(self, index: u8) -> bool {
        self.0[(index - 2) as usize] == 0
    }

    pub fn is_finished(self) -> bool {
        let mut ok = 0;
        for &c in self.0.iter() {
            if c == 0 {
                ok += 1;
            }
        }
        ok >= 3
    }

    pub fn get_moved(self, dice_pair: DicePair, climb: Climbing) -> MovedState {
        fn apply(mut gain: Gain, climb: Climbing, dice: u8) -> Option<DicingState> {
            if gain.is_climbed(dice) {
                None
            } else if let Some(climb) = climb.apply(dice) {
                gain[dice] -= 1;
                Some(DicingState { gain, climb })
            } else {
                None
            }
        }

        let a0 = apply(self, climb, dice_pair.first());
        if let Some(a) = a0
            .clone()
            .and_then(|d| apply(d.gain, d.climb, dice_pair.second()))
        {
            MovedState::Deterministic(a)
        } else {
            let a1 = apply(self, climb, dice_pair.second());
            match (a0, a1) {
                (None, None) => MovedState::None,
                (Some(a), None) | (None, Some(a)) => MovedState::Deterministic(a),
                (Some(a0), Some(a1)) => MovedState::Which(a0, a1),
            }
        }
    }

    pub fn inner(&self) -> &[u8; 11] {
        &self.0
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum MovedState {
    None,
    Deterministic(DicingState),
    Which(DicingState, DicingState),
}

#[derive(Debug, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct DicingState {
    pub gain: Gain,
    pub climb: Climbing,
}

impl DicingState {
    #[inline]
    pub const fn new(gain: Gain) -> DicingState {
        DicingState {
            gain,
            climb: Climbing::Zero,
        }
    }

    pub fn list_all_possibilities(&self) -> FnvHashMap<Vec<DicingState>, IntProb> {
        let mut m = FnvHashMap::with_capacity_and_hasher(128, Default::default());
        for dice_info in DICE_INFO.iter() {
            let mut v = self.list_can(&dice_info.dice_pairs);
            v.sort();
            *m.entry(v).or_insert(0) += dice_info.probability;
        }
        m
    }

    /// サイコロを振ったときに取りうる選択肢を全列挙する。
    /// 4つのサイコロからサイコロペアのリストを作成する作業が別途必要。
    ///
    /// ```rust
    /// # use cant_stop::{DicingState, Gain, DicePair};
    /// let state = DicingState::new(Gain::new());
    /// assert_eq!(state.list_can(&[DicePair::new(2, 2)]).len(), 1);
    /// ```
    pub fn list_can(&self, dice_pairs: &[DicePair]) -> Vec<DicingState> {
        let mut v = Vec::new();
        for &dice in dice_pairs {
            let moved = self.gain.get_moved(dice, self.climb);
            match moved {
                MovedState::None => (),
                MovedState::Deterministic(a) => v.push(a),
                MovedState::Which(a, b) => {
                    v.push(a);
                    v.push(b);
                }
            }
        }
        v
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::MovedState;

    #[test]
    fn climbing() {
        assert_eq!(Climbing::Zero.apply(4), Some(Climbing::One(4)));
        assert_eq!(Climbing::One(4).apply(4), Some(Climbing::One(4)));
        assert_eq!(Climbing::One(4).apply(5), Some(Climbing::Two(4, 5)));
        assert_eq!(Climbing::One(4).apply(2), Some(Climbing::Two(2, 4)));
        assert_eq!(Climbing::Two(4, 7).apply(4), Some(Climbing::Two(4, 7)));
        assert_eq!(Climbing::Two(4, 7).apply(7), Some(Climbing::Two(4, 7)));
        assert_eq!(Climbing::Two(4, 7).apply(5), Some(Climbing::Three(4, 5, 7)));
        assert_eq!(Climbing::Two(4, 7).apply(3), Some(Climbing::Three(3, 4, 7)));
        assert_eq!(Climbing::Two(4, 7).apply(9), Some(Climbing::Three(4, 7, 9)));
        for d in vec![4, 7, 9] {
            assert_eq!(
                Climbing::Three(4, 7, 9).apply(d),
                Some(Climbing::Three(4, 7, 9)),
            );
        }
        for d in vec![2, 5, 6, 8, 10, 11] {
            assert_eq!(Climbing::Three(4, 7, 9).apply(d), None);
        }
    }

    #[test]
    fn is_climbed() {
        let mut board = Gain::new();
        for d in 2..=12 {
            assert!(!board.is_climbed(d));
        }
        board[2] = 0;
        board[11] = 0;
        assert!(board.is_climbed(2));
        assert!(board.is_climbed(11));
    }

    #[test]
    fn is_finished() {
        let mut board = Gain::new();
        assert!(!board.is_finished());
        board[2] = 0;
        board[11] = 0;
        assert!(!board.is_finished());
        board[7] = 0;
        assert!(board.is_finished());
        board[8] = 0;
        assert!(board.is_finished());
    }

    /// 空の状態から動かしたら期待通りに動く
    #[test]
    fn get_moved_from_empty() {
        let mut gain = Gain::new();
        let next = gain.get_moved(DicePair::new(5, 6), Climbing::Zero);
        gain[5] -= 1;
        gain[6] -= 1;
        assert_eq!(
            next,
            MovedState::Deterministic(DicingState {
                gain,
                climb: Climbing::Two(5, 6)
            })
        );
    }

    /// どっちかだけ
    #[test]
    fn get_moved_p2() {
        let mut gain = Gain::new();
        gain[2] -= 1;
        gain[3] -= 1;
        let next = gain.get_moved(DicePair::new(5, 6), Climbing::Two(2, 3));
        let mut g1 = gain;
        g1[5] -= 1;
        let mut g2 = gain;
        g2[6] -= 1;
        assert_eq!(
            next,
            MovedState::Which(
                DicingState {
                    gain: g1,
                    climb: Climbing::Three(2, 3, 5)
                },
                DicingState {
                    gain: g2,
                    climb: Climbing::Three(2, 3, 6)
                },
            ),
        );
    }

    #[test]
    fn get_moved_same() {
        let mut gain = Gain::new();
        let next = gain.get_moved(DicePair::new(4, 4), Climbing::Zero);
        gain[4] -= 2;
        assert_eq!(
            next,
            MovedState::Deterministic(DicingState {
                gain,
                climb: Climbing::One(4)
            })
        );
    }

    #[test]
    fn get_moved_climbed() {
        let mut gain = Gain::new();
        gain[6] = 0;
        let next = gain.get_moved(DicePair::new(6, 7), Climbing::Zero);
        gain[7] -= 1;
        assert_eq!(
            next,
            MovedState::Deterministic(DicingState {
                gain,
                climb: Climbing::One(7)
            })
        );
    }

    #[test]
    fn smoke_list_next() {
        let state = DicingState::new(Gain::new());
        let next = state.list_all_possibilities();
        assert_eq!(next.len(), DICE_INFO.len());
        assert_eq!(next.values().sum::<u64>(), 6 * 6 * 6 * 6);
    }
}
