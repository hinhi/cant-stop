use rand::Rng;

use crate::{
    board::{DicingState, Gain},
    roll_dice,
};

#[derive(Debug, Clone)]
pub struct Game {
    dicing_state: DicingState,
    pub dicing_player: usize,
    players: Vec<Gain>,
}

impl Game {
    pub fn new(n: usize) -> Game {
        assert!(n > 0);
        Game {
            dicing_state: DicingState::new(Gain::new()),
            dicing_player: 0,
            players: vec![Gain::new(); n],
        }
    }

    pub fn list_can<R: Rng>(&self, rng: &mut R) -> Vec<DicingState> {
        self.dicing_state.list_can(roll_dice(rng))
    }

    pub fn decide(&mut self, decision: PlayerDecision) {
        match decision {
            PlayerDecision::Cant => {
                self.dicing_player = (self.dicing_player + 1) % self.players.len();
                self.dicing_state = DicingState::new(self.players[self.dicing_player]);
            }
            PlayerDecision::Stop(state) => {
                self.players[self.dicing_player] = state.gain;
                self.dicing_player = (self.dicing_player + 1) % self.players.len();
                self.dicing_state = DicingState::new(self.players[self.dicing_player]);
            }
            PlayerDecision::Go(state) => {
                self.dicing_state = state;
            }
        }
    }

    pub fn winner(&self) -> Option<usize> {
        for (i, player) in self.players.iter().enumerate() {
            if player.is_finished() {
                return Some(i);
            }
        }
        None
    }
}

#[derive(Debug, Clone)]
pub enum PlayerDecision {
    Cant,
    Stop(DicingState),
    Go(DicingState),
}
